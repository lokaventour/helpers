module gitlab.com/lokaventour/helpers

go 1.13

require (
	github.com/apex/log v1.9.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/golang/protobuf v1.4.1
	github.com/mailgun/mailgun-go v2.0.0+incompatible
	github.com/mailgun/mailgun-go/v3 v3.6.4
	github.com/pusher/push-notifications-go v0.0.0-20200210154345-764224c311b8 // indirect
	github.com/pusher/pusher-http-go v4.0.1+incompatible
	github.com/segmentio/kafka-go v0.3.7
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/lokaventour/proto v0.0.0-20200928062224-bc0229ef6a91
	google.golang.org/protobuf v1.25.0
)
